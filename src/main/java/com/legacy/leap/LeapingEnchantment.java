package com.legacy.leap;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraftforge.registries.ForgeRegistries;

public class LeapingEnchantment extends Enchantment
{
	public LeapingEnchantment(Enchantment.Rarity rarityIn, EquipmentSlot... slots)
	{
		super(rarityIn, EnchantmentCategory.ARMOR_FEET, slots);
	}

	@Override
	public int getMinCost(int enchantmentLevel)
	{
		return enchantmentLevel * 25;
	}

	@Override
	public int getMaxCost(int enchantmentLevel)
	{
		return this.getMinCost(enchantmentLevel) + 50;
	}

	@Override
	public boolean isTreasureOnly()
	{
		return true;
	}

	@Override
	public int getMaxLevel()
	{
		return 1;
	}

	@Override
	public boolean checkCompatibility(Enchantment ench)
	{
		ResourceLocation key;
		return super.checkCompatibility(ench) && ench != Enchantments.DEPTH_STRIDER && ench != Enchantments.FROST_WALKER && !((key = ForgeRegistries.ENCHANTMENTS.getKey(ench)) != null && key.getPath().endsWith("stepping"));
	}
}
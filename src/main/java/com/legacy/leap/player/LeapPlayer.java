package com.legacy.leap.player;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.leap.LeapRegistry;
import com.legacy.leap.player.util.ILeapPlayer;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.NonNullSupplier;
import net.minecraftforge.event.entity.living.LivingFallEvent;

public class LeapPlayer implements ILeapPlayer
{
	public static final Capability<ILeapPlayer> INSTANCE = CapabilityManager.get(new CapabilityToken<>()
	{
	});

	private Player player;
	private boolean doubleJumped = false;

	public LeapPlayer()
	{
	}

	public LeapPlayer(Player player)
	{
		super();
		this.player = player;
	}

	@Nullable
	public static ILeapPlayer get(Player player)
	{
		return LeapPlayer.getIfPresent(player, (skyPlayer) -> skyPlayer);
	}

	public static <E extends Player> void ifPresent(E player, Consumer<ILeapPlayer> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			action.accept(player.getCapability(INSTANCE).resolve().get());
	}

	@Nullable
	public static <E extends Player, R> R getIfPresent(E player, Function<ILeapPlayer, R> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return null;
	}

	public static <E extends Player, R> R getIfPresent(E player, Function<ILeapPlayer, R> action, NonNullSupplier<R> elseSupplier)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return elseSupplier.get();
	}

	@Override
	public CompoundTag writeAdditional(CompoundTag compound)
	{
		compound.putBoolean("UsedDoubleJump", this.hasDoubleJumped());

		return compound;
	}

	@Override
	public void read(CompoundTag compound)
	{
		this.doubleJumped = compound.getBoolean("UsedDoubleJump");
	}

	@Override
	public Player getPlayer()
	{
		return this.player;
	}

	@Override
	public boolean hasDoubleJumped()
	{
		return this.doubleJumped;
	}

	@Override
	public void setDoubleJumping(boolean used, double motionX, double motionZ)
	{
		if (used)
			this.onDoubleJumped(motionX, motionZ);
		else
			this.doubleJumped = false;
	}

	public void onDoubleJumped(double motionX, double motionZ)
	{
		// hacky way of limiting speed server-side just in case
		float max = 0.30F;
		if (motionX > max)
			motionX = max;

		if (motionX < -max)
			motionX = -max;

		if (motionZ > max)
			motionZ = max;

		if (motionZ < -max)
			motionZ = -max;

		Player entity = this.getPlayer();
		float enchantmentLevel = EnchantmentHelper.getEnchantmentLevel(LeapRegistry.LEAPING.get(), entity);
		boolean canJump = !entity.isOnGround() && !this.hasDoubleJumped() && entity.getDeltaMovement().y() < 0;

		if (enchantmentLevel <= 0)
			return;

		// if the player can jump, jump
		if (canJump && !entity.isFallFlying() && !entity.getAbilities().flying)
		{
			entity.setDeltaMovement(motionX, 0.6D, motionZ);
			entity.hurtMarked = true;

			entity.level.playSound(null, entity.blockPosition(), LeapRegistry.LEAP_SOUND.get(), SoundSource.PLAYERS, 0.3F, 2.0F);

			if (entity.level instanceof ServerLevel sl)
			{
				for (int i = 0; i < 20; ++i)
				{
					double d0 = sl.random.nextGaussian() * 0.02D;
					double d1 = sl.random.nextGaussian() * 0.02D;
					double d2 = sl.random.nextGaussian() * 0.02D;

					sl.sendParticles(ParticleTypes.POOF, entity.getX() + (double) (entity.level.random.nextFloat() * entity.getBbWidth() * 2.0F) - (double) entity.getBbWidth() - d0 * 10.0D, entity.getY() - d1 * 10.0D, entity.getZ() + (double) (entity.level.random.nextFloat() * entity.getBbWidth() * 2.0F) - (double) entity.getBbWidth() - d2 * 10.0D, 1, d0, d1, d2, 0.0F);
				}
			}

			this.doubleJumped = true;
		}
	}

	@Override
	public void onFall(LivingFallEvent event)
	{
		float enchantmentLevel = EnchantmentHelper.getEnchantmentLevel(LeapRegistry.LEAPING.get(), this.getPlayer());

		if (this.hasDoubleJumped())
		{
			if (enchantmentLevel > 0)
			{
				int i = Mth.ceil((event.getDistance() - 3.0F - enchantmentLevel) * event.getDamageMultiplier());
				event.setDistance(i);
			}

			this.setDoubleJumping(false, 0, 0);
		}
	}
}

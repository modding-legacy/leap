package com.legacy.leap.player.util;

import com.legacy.leap.player.LeapPlayer;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.core.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

public class CapabilityProvider implements ICapabilitySerializable<CompoundTag>
{
	private final LazyOptional<ILeapPlayer> playerHandler;

	public CapabilityProvider(ILeapPlayer skyPlayer)
	{
		this.playerHandler = LazyOptional.of(() -> skyPlayer);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		return cap == LeapPlayer.INSTANCE ? this.playerHandler.cast() : LazyOptional.empty();
	}

	@Override
	public void deserializeNBT(CompoundTag compound)
	{
		this.playerHandler.orElse(null).read(compound);
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		this.playerHandler.orElse(null).writeAdditional(compound);
		return compound;
	}
}
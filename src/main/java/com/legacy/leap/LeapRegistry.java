package com.legacy.leap;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantment.Rarity;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

@EventBusSubscriber(modid = LeapMod.MODID, bus = Bus.MOD)
public class LeapRegistry
{
	private static final ResourceLocation LEAP_KEY = LeapMod.locate("entity.player.ench_leap");

	public static final Lazy<Enchantment> LEAPING = Lazy.of(() -> new LeapingEnchantment(Rarity.RARE, EquipmentSlot.FEET));
	public static final Lazy<SoundEvent> LEAP_SOUND = Lazy.of(() -> SoundEvent.createVariableRangeEvent(LEAP_KEY));

	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(ForgeRegistries.Keys.ENCHANTMENTS))
			event.register(ForgeRegistries.Keys.ENCHANTMENTS, LeapMod.locate("leaping"), LEAPING);
		else if (event.getRegistryKey().equals(ForgeRegistries.Keys.SOUND_EVENTS))
			event.register(ForgeRegistries.Keys.SOUND_EVENTS, LEAP_KEY, LEAP_SOUND);
	}
}
